package time_it

import "core:time"
import "core:mem"
import "core:fmt"

Duration :: time.Duration

SampleBuffer :: [dynamic]Duration

Benchmark :: struct {
	samples: map[string]SampleBuffer,
	allocator: mem.Allocator,
}

// Initializes a benchmark with an allocator
benchmark_init :: proc(b: ^Benchmark, allocator := context.allocator){
	b.allocator = allocator
	b.samples = make(map[string]SampleBuffer, allocator = allocator)
}

// Destroys benchmark, using its internal allocator
benchmark_destroy :: proc(b: ^Benchmark){
	context.allocator = b.allocator
	for name in b.samples {
		delete(b.samples[name])
	}
	delete(b.samples)
}

// Prints a human readable summary of benchmark
summary :: proc(b: Benchmark){
	collect_measurements :: proc(s: []Duration) -> (mini, sum, maxi: Duration) {
		mini = max(Duration)
		maxi = min(Duration)
		sum = Duration(0)
		for v in s {
			maxi = max(maxi, v)
			mini = min(mini, v)
			sum += v
		}
		return mini, sum, maxi
	}


	total :: proc(s: []Duration) -> Duration {
		acc := Duration(0)
		for v in s {
			acc += v
		}
		return acc
	}

	fmt.printfln("name: min | avg | max")
	for name in b.samples {
		mini, sum, maxi := collect_measurements(b.samples[name][:])
		fmt.printfln("%s: %v | %v | %v", name, mini, sum / Duration(len(b.samples[name])), maxi)
	}
}

// Create a sample for benchmark, this automatically closes with scope. Example:
/*
   {
	   time_it.sample(&bench, "Outer scope")
	   inner_scope: for _ in 0..<1000 {
		   time_it.sample(&bench, "Inner scope")
	   }
   }
   summary(benchmark)
 */
@(deferred_in_out=sample_done)
sample :: proc(b: ^Benchmark, key: string) -> (t: time.Tick) {
	return time.tick_now()
}

// Finishes a sample, interal use only.
@private
sample_done :: proc(b: ^Benchmark, key: string, t: time.Tick){
	elapsed := time.tick_since(t)
	context.allocator = b.allocator
	if key not_in b.samples {
		b.samples[key] = make(SampleBuffer)
	}
	append(&b.samples[key], elapsed)
}
